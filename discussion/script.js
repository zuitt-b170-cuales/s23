// QUERY OPERATORS

// $gt - greater than 
// {field: {$gt: value}}
/*
	SYNTAX:
		db.users.find(
			{
				age: {$gt: 50}
			}
		);
*/